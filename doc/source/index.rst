Welcome to networking-l2gw-tempest-plugin's documentation!
============================================================

Tests for the
`Networking-l2gw project <https://docs.openstack.org/networking-l2gw/latest/>`_.

It provides Networking-l2gw scenario tests framework and tempest tests.

User guide
----------

**Tempest Plugin**

.. toctree::
   :maxdepth: 1

   tempest-plugin
   readme
   contributing
   releasenotes

Source
------

* License: Apache License, Version 2.0
* `PyPi`_ - package installation
* `Launchpad project`_ - release management
* `Blueprints`_ - feature specifications
* :doc:`releasenotes`

.. _PyPi: https://pypi.org/project/networking-l2gw-tempest-plugin
.. _Launchpad project: https://launchpad.net/networking-l2gw-tempest-plugin
.. _Blueprints: https://blueprints.launchpad.net/networking-l2gw-tempest-plugin

Indices and tables
------------------

* :ref:`genindex`
* :ref:`search`

